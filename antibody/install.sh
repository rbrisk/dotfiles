#!/bin/sh
if command -v brew >/dev/null 2>&1; then
	brew tap | grep -q 'getantibody/tap' || brew tap getantibody/tap
	brew install antibody
else
	if [ ! -d $HOME/.local/bin ]; then
		mkdir -p $HOME/.local/bin
	fi
	curl -sL https://git.io/antibody | sh -s -- -b $HOME/.local/bin
fi
antibody bundle <"$DOTFILES/antibody/bundles.txt" >~/.zsh_plugins.sh
antibody update
