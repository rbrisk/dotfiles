# this block is in alphabetical order
ael-code/zsh-colored-man-pages
caarlos0/ports kind:path
mafredri/zsh-async
robbyrussell/oh-my-zsh path:plugins/colorize
zsh-users/zsh-autosuggestions
zsh-users/zsh-completions
#zsh-users/zsh-syntax-highlighting


# these should be the last ones!
sindresorhus/pure
zdharma-continuum/fast-syntax-highlighting
zsh-users/zsh-history-substring-search
