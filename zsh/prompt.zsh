#!/bin/zsh
PURE_PROMPT_SYMBOL="Ф❯"
PURE_PROMPT_VICMD_SYMBOL="Н❯"
export REPORTTIME=10
zstyle :prompt:pure:path color "#e69f1f"
zstyle :prompt:pure:user color "#01b8ff"
zstyle :prompt:pure:host color "#01b8ff"
zstyle :prompt:pure:git:branch color "#0040ff"
zstyle :prompt:pure:git:action color "#0040ff"
zstyle :prompt:pure:git:continuation color "#0040ff"
zstyle :prompt:pure:prompt:error color "#ffff00"
zstyle :prompt:pure:prompt:success color "#55ffff"
