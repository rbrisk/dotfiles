set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/syntastic'
Plugin 'godlygeek/tabular'
Plugin 'tpope/vim-surround'
" Plugin 'Valloric/YouCompleteMe'
" All colour schemes from vim.org
"Plugin 'flazz/vim-colorschemes'
"Plugin 'vim-pandoc/vim-pandoc'
"Plugin 'vim-pandoc/vim-pandoc-syntax'
"Alignment, similar to tabular
"Plugin 'junegunn/vim-easy-align'
"Plugin 'vim-python/python-syntax'
Plugin 'airblade/vim-gitgutter'
"Plugin 'derekwyatt/vim-scala'
Plugin 'tpope/vim-sensible'
Plugin 'tpope/vim-sleuth'
Plugin 'vim-airline/vim-airline'
Plugin 'preservim/nerdtree'
call vundle#end()
filetype plugin indent on

runtime! plugin/sensible.vim
"set backspace=indent,eol,start  " Makes backspace key more powerful.
"set incsearch                   " Search as you type

"
" Settings
"
set noerrorbells                " No beeps
" set autoindent                " Overridden by vim-sleuth
set number                      " Show line numbers
set numberwidth=4
set ruler
set showcmd                     " Show me what I'm typing
set noshowmode                  " Show current mode.
set hlsearch                    " Highlight search
" Remove search highlight
nnoremap <leader><space> :nohlsearch<CR>
set nobackup                    " Don't create annoying backup files
set splitbelow                  " Split horizontal windows below to the current windows
set splitright                  " Split vertical windows right to the current windows
set encoding=utf-8              " Set default encoding to UTF-8
syntax sync minlines=256
set synmaxcol=200               " Stop syntax highlighting at this char position
set conceallevel=0              " do not hide markdown
set laststatus=2                " Always show status line
" Make Vim to handle long lines nicely.
set textwidth=100
set colorcolumn=101
" hi ColorColumn ctermbg=darkblue
set formatoptions=cq
set list

set showmatch
set smarttab

set shiftround                  " Round indent to multiple of 'shiftwidth'

syntax on
colorscheme pysar

" let g:pandoc#filetypes#handled = ["pandoc", "markdown"]
" Enable code highlighting in markdown
hi def link markdownCode Statement
" These will prevent the argument alignment in R files
let r_indent_align_args = 0
let r_indent_ess_comments = 0
let r_indent_ess_compatible = 0
" python
let g:python_recommended_style = 0
let g:python_highlight_all = 1
" markdown
let g:markdown_fenced_languages = ['html', 'python', 'bash=sh']
inoremap jj <ESC>
set timeout
set timeoutlen=200
map <S-Enter> O<ESC>
map <Enter> o<ESC>
" Ukrainian keymap Ctrl+^ (insert and search modes only)
set keymap=ukrainian-jcuken
set iminsert=0
set imsearch=0
" Tabular hot key, leader is \ but needs to be pressed for a few seconds to register
nmap <leader>a :Tabularize /
" Spelling languages
set spelllang=en_gb,uk,de_ch

" In many terminal emulators the mouse works just fine, thus enable it.
"if has('mouse')
"	set mouse=a
"endif

" If linux then set ttymouse
"let s:uname = system("echo -n \"$(uname)\"")
"if !v:shell_error && s:uname == "Linux" && !has('nvim')
"	set ttymouse=xterm
"endif

" Fast saving
nmap <leader>w :w!<cr>

" Center the screen
nnoremap <space> zz

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

" Do not show stupid q: window
map q: :q

" never do this again --> :set paste <ctrl-v> :set no paste
let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

function! XTermPasteBegin()
	set pastetoggle=<Esc>[201~
	set paste
	return ""
endfunction

"
" File Type settings
"

augroup filetypedetect
	au BufNewFile,BufRead .tmux.conf*,tmux.conf* setf tmux
	au BufNewFile,BufRead .nginx.conf*,nginx.conf* setf nginx
augroup END

au FileType nginx setlocal ts=3 sw=3 sts=3
au BufNewFile,BufRead *.txt setlocal spell
au BufNewFile,BufRead *.md setlocal spell
"au BufNewFile,BufRead *.txt setlocal spell ts=3 sw=3
"au BufNewFile,BufRead *.md setlocal spell ts=3 sw=3
"au BufNewFile,BufRead *.yml,*.yaml setlocal expandtab ts=2 sw=2
"au BufNewFile,BufRead *.json setlocal expandtab ts=2 sw=2
"au BufNewFile,BufReadPost *.scala setlocal sw=3 ts=3 sts=3
"au BufNewFile,BufRead *.lua setlocal ts=3 sw=3 sts=3

function! Tabs(width, ...)
	let &shiftwidth=a:width
	let &tabstop=a:width
	let &softtabstop=a:width
	if (a:0 > 0 && a:1 == 1)
		set expandtab
	else
		set noexpandtab
	endif
endfunction

" === vim-json ===
let g:vim_json_syntax_conceal = 0

" === vim-airline ===
let g:airline_detect_spell = 0           " Do not show spell mode
let g:airline_detect_spelllang = 0       " Do not show spell lang
let g:airline_detect_iminsert = 1        " keymap insert detection
let g:airline_powerline_fonts = 1        " Use powerline fonts
let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]' " Do not show default filetype
let g:airline#extensions#keymap#enabled = 0                  " Do not show keymap
let g:airline_mode_map = {
	\ 's'      : 'вибір',
	\ 'V'      : 'наоч.',
	\ 'ni'     : 'вставка',
	\ 'ic'     : 'вставка',
	\ 'R'      : 'заміна',
	\ '^S'     : 'вибір',
	\ 'no'     : 'очік.',
	\ '^V'     : 'наоч.',
	\ 'multi'  : 'мульті',
	\ '__'     : '------',
	\ 'Rv'     : 'наоч.',
	\ 'c'      : 'наказ',
	\ 'ix'     : 'вставка',
	\ 'i'      : 'вставка',
	\ 'n'      : 'звич.',
	\ 'S'      : 'вибір',
	\ 't'      : 'терм.',
	\ 'v'      : 'наоч.'
	\ }
let g:airline_section_z = '%p%% :%v'

" === vim-sleuth ===
" Default tab width for projects with tabs
set tabstop=3
" vim:ts=3:sw=3:noexpandtab
