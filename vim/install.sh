#!/bin/sh
prefix=~/.vim/bundle/Vundle.vim
if [ ! -e "$prefix" ]; then
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
	vim +PluginInstall +qall
fi
vim +PluginUpdate +qall

