#!/bin/sh
if [ -x /usr/bin/dircolors ]; then
	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
	alias xzgrep='xzgrep --color=auto'
	alias zgrep='zgrep --color=auto'
fi

alias xx='exit'

# open
if [ -e /usr/bin/xdg-open ]; then
	alias open="xdg-open"
fi
