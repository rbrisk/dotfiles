#!/bin/sh
test -L ~/.tmux.conf || {
	if [ -f ~/.tmux.conf ]; then
		mv ~/.tmux.conf ~/.tmux.conf.bak
	fi
	ln -s "$DOTFILES"/tmux/tmux.conf.symlink ~/.tmux.conf
}
