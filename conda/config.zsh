#!/bin/sh

export MAMBA_NO_BANNER=1
# Contrary to the docs, conda does not detect it without the env var
export CONDARC=~/.config/conda/condarc
