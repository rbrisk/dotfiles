#!/bin/sh
test -L ~/.condarc || {
	if [ -f ~/.condarc ]; then
		mv ~/.condarc ~/.condarc.bak
	fi
	ln -s "$DOTFILES"/conda/condarc.symlink ~/.condarc
}
