# dotfiles

This is a heavily modified copy of [carlos' dotfiles](https://github.com/caarlos0/dotfiles). Many
dotfiles (GO, Java, etc.) were removed while some others (tmux, conda, etc.) were added. The
original readme is below.

## Notes

If you install this on a server that run loads something via `profile.d`, e.g. lmod or slurm, you
need to source them in `~/.localrc`. That is also the place for adding various aliases that are
specific for the location.

There are some issues with [FPATH](https://stackoverflow.com/a/71866174) as described in [LMOD
FAQ](https://lmod.readthedocs.io/en/latest/040_FAQ.html). If you try starting `zsh` from `bash`, you
will most likely get an error like `compinit: function definition file not found`. To avoid it, you
can try resetting `FPATH` before starting `zsh` or bypass `bash` by setting `RequestTTY yes` and
`RemoteCommand zsh` for the ssh connection.


# carlos' dotfiles

[![Build Status][tb]][tp]
[![Powered by Antibody][ab]][ap]

> Config files for ZSH, Java, Ruby, Go, Editors, Terminals and more.

![screenshot 1][scrn1]

![screenshot 2][scrn2]

[ap]: https://github.com/getantibody/antibody
[ab]: https://img.shields.io/badge/powered%20by-antibody-blue.svg?style=flat-square
[tb]: https://img.shields.io/travis/caarlos0/dotfiles/master.svg?style=flat-square
[tp]: https://travis-ci.org/caarlos0/dotfiles
[scrn1]: /docs/screenshot1.png
[scrn2]: /docs/screenshot2.png

## Installation

### Dependencies

First, make sure you have all those things installed:

- `git`: to clone the repo
- `curl`: to download some stuff
- `tar`: to extract downloaded stuff
- `zsh`: to actually run the dotfiles
- `sudo`: some configs may need that

### Install

Then, run these steps:

```console
$ git clone https://github.com/caarlos0/dotfiles.git ~/.dotfiles
$ cd ~/.dotfiles
$ ./script/bootstrap
$ zsh # or just close and open your terminal again.
```

> All changed files will be backed up with a `.backup` suffix.

### Recommended Software

For macOS, I recommend:

- iTerm: a better terminal emulator;

For both Linux and macOS:

- [`diff-so-fancy`](https://github.com/so-fancy/diff-so-fancy):
better git difs (you'll need to run `dot_update` to apply it);
- [`fzf`](https://github.com/junegunn/fzf):
fuzzy finder, used in `,t` on vim, for example;
- [`kubectx`](https://github.com/ahmetb/kubectx) for better kubernetes context
  and namespace switch;

### macOS defaults

You use it by running:

```console
$DOTFILES/macos/set-defaults.sh
```

And logging out and in again/restart.

### Themes and fonts being used

Theme is **[Dracula](https://draculatheme.com)**, font is **JetBrains Mono** on
editors and **Hack** on terminals.

## Further help:

- [Personalize your configs](/docs/PERSONALIZATION.md)
- [Understand how it works](/docs/PHILOSOPHY.md)
- [License](/LICENSE.md)

## Contributing

Feel free to contribute. Pull requests will be automatically
checked/linted with [Shellcheck](https://github.com/koalaman/shellcheck)
and [shfmt](https://github.com/mvdan/sh).

