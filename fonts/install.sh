#!/bin/sh

if [ -z "$TMPDIR" ]; then
	export TMPDIR=/tmp
fi

git clone --depth=1 https://github.com/powerline/fonts.git $TMPDIR/fonts
$TMPDIR/fonts/install.sh

if [ -d "$TMPDIR/fonts" ]; then
	rm -rf "$TMPDIR/fonts"
fi

